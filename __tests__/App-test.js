/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
import contactValidation from '../src/validation/contactValidation'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

// it('renders correctly', () => {
//   renderer.create(<App />);
// });

test('contact form validation', () => {
  expect(contactValidation('firstname','lastname',0,'imageurl')).toEqual('')
  expect(contactValidation('','lastname',0,'imageurl')).toEqual('please insert first name...')
  expect(contactValidation('firstname','',0,'imageurl')).toEqual('please insert last name...')
  expect(contactValidation('firstname','lastname','','imageurl')).toEqual('please insert age...')
  expect(contactValidation('firstname','lastname',101,'imageurl')).toEqual('age can not be more than 100')
  expect(contactValidation('firstname','lastname',0,undefined)).toEqual('please select your image...')
})