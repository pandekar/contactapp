import axios from 'axios'

const BASE_URL = 'https://simple-contact-crud.herokuapp.com/contact'

const getContacts = async () => {
    try{
        const {data} = await axios.get(BASE_URL)
        return data
    }catch(error){
        throw error.data
    }
}

const getContact = async (id) => {
    try{
        const {data} = await axios.get(BASE_URL+'/'+id)
        return data
    }catch(error){
        throw error.data
    }
}

const postContact = async (contact) => {
    try{
        const {data} = await axios.post(BASE_URL, contact)
        return data
    }catch(error){
        throw error.data
    }
}

const putContact = async (contact, id) => {
    try{
        const {data} = await axios.put(BASE_URL+'/'+id, contact)
        return data
    }catch(error){
        throw error.data
    }
}

const deleteContact = async (id) => {
    try{
        const {data} = await axios.delete(BASE_URL+'/'+id)
        return data
    }catch(error){
        throw error
    }
}

export {getContacts, getContact, postContact, putContact, deleteContact}