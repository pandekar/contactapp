import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import * as screens from '../screens'

function Router(){

    const Stack = createStackNavigator();

    return(
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: '#ec131c',
                },
                headerTintColor: '#fff',
            }} >
            <Stack.Screen name="Home" component={screens.Homepage} 
                  options = {{
                    headerShown: false
                  }} />
            <Stack.Screen name="contact_add" component={screens.ContactAdd}
                  options = {{
                    title: 'back'
                  }} />
            <Stack.Screen name="contact_detail" component={screens.ContactDetail}
                  options = {{
                    title: 'back'
                  }} />
        </Stack.Navigator>
    )
}

export default Router