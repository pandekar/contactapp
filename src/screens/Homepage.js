import React, {useCallback} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, FlatList, Image, Alert, ToastAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useRoute, useNavigation, useFocusEffect } from '@react-navigation/native';
import { getContacts, deleteContact } from '../services/contact.service';
import { useDispatch, useSelector } from 'react-redux';
import { ClearContacts, InitiateContacts, DeleteContact, fetchInitiateContact, reloadContacts } from '../redux/actions/contact.action';

const Homepage = () => {

    const route = useRoute()
    const dispatch = useDispatch()
    const navigation = useNavigation()
    // const contacts = useSelector((state) => state.contactReducer)

    //redux saga
    const contacts = useSelector(state => state.contactReducer.data)
    const contactsLoading = useSelector(state => state.contactReducer.loading)
    const error = useSelector(state => state.contactReducer.error)

    const addContact = () => {
        navigation.navigate('contact_add')
    }
    
    const refreshContacts = () => {
        // dispatch(ClearContacts())
        dispatch(reloadContacts())
    }

    const updateContact = (id) => {
        const contact =  {
            msg : 'update',
            id : id
        }
        navigation.navigate('contact_add', contact)
    }

    const details = (id) => {
        navigation.navigate('contact_detail', id)
    }

    const deleteAlert = (id) =>
            Alert.alert(
            "delete contact?",
            "",
            [
                {
                    text: "Cancel",
                    style: "cancel"
                },
                { text: "OK", onPress: () => {
                    deleteContact(id).then(response => {
                        const showToast = (text) => {
                            ToastAndroid.show(text, ToastAndroid.SHORT)
                        }
                        if(response.message === "contact deleted"){
                            const contact = {
                                id : id
                            }
                            dispatch(DeleteContact(contact))
                            showToast(response.message)
                        }
                    }, (error) => {
                        ToastAndroid.show(error.response.data.message, ToastAndroid.SHORT)
                    })
                }}
            ]
        );

    useFocusEffect(useCallback(() => {
        if(!contacts.length){
            // getContacts().then(res => {
            //     dispatch(InitiateContacts(res.data))
            // })
            dispatch(fetchInitiateContact())
        }
    }, [contacts]))

    return (
        <View style={styles.main}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Contact List</Text>
                <TouchableOpacity >
                    <Icon   
                        name="refresh"
                        size={30} 
                        color="white"
                        onPress={() => refreshContacts()}
                    />
                </TouchableOpacity>
                <TouchableOpacity >
                    <Icon   
                        name="plus"
                        size={30} 
                        color="white"
                        onPress={() => addContact()}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.listBox}>
                {contactsLoading && <Text style={{ fontSize: 20 }}>Loading...</Text>}
                {error && <Text>{error}</Text>}
                {contacts && <FlatList
                    data={contacts}
                    renderItem={({item}) => 
                        <View style={styles.contact}>
                            <TouchableOpacity onPress={() => details(item.id)}>
                                <View>
                                    {item.photo != "N/A" 
                                        ?   
                                            <Image
                                                style={styles.image}
                                                source={{
                                                uri: item.photo,
                                                }}
                                            />
                                        :   
                                            <Image
                                                style={styles.image} 
                                                source={require('../images/unknown.png')} 
                                            />
                                            
                                    }
                                </View>
                            </TouchableOpacity>
                            <View>
                                <View style={styles.itemContent}>
                                    <Text style={styles.name}>{item.firstName} {item.lastName}</Text>
                                    <Text style={{ fontSize: 15 }}>age: {item.age}</Text>
                                </View>
                                <View style={styles.buttonContainer}>
                                    <View>
                                        <TouchableOpacity style={styles.button} onPress={() => updateContact(item.id)}>
                                            <Icon name="pencil" size={25} color="gray"/>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={styles.button}>
                                            <Icon name="remove" size={25} color="firebrick" onPress={() => deleteAlert(item.id)}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    }
                    keyExtractor={item => item.id}
                />}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: 'whitesmoke',
    },
    listBox:{
        flex: 1,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    button: {
        margin: 10
    },
    contact: {
        padding: 15,
        margin: 10,
        borderRadius: 25,
        flexDirection: 'row',
        backgroundColor: '#FFF5E2'
    },
    header: {
        paddingLeft: 40,
        paddingRight: 40,
        height: 50,
        borderBottomStartRadius:25,
        borderBottomEndRadius:25,
        backgroundColor: '#ec131c',
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    headerText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20
    },
    name:{
        fontSize: 24,
        fontWeight: 'bold'
    },
    image: {
      width: 100,
      height: 100,
      borderRadius: 25,
      marginRight: 20
    },
    logo: {
      width: 66,
      height: 58,
    },
    itemContent:{
        alignContent: 'flex-start',
    },
  });

export default Homepage