import React ,{useEffect, useState} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Image, ToastAndroid, ScrollView, ActivityIndicator } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux'
import { launchImageLibrary } from 'react-native-image-picker'
import storage from '@react-native-firebase/storage'
import contactValidation from '../validation/contactValidation'
import {postContact, putContact} from '../services/contact.service'
// import { ClearContacts } from '../redux/actions/contact.action'
import { reloadContacts } from '../redux/actions/contact.action'

const ContactAdd = () => {

    const dispatch = useDispatch()
    const route = useRoute()
    const navigation = useNavigation()
    const contacts = useSelector((state) => state.contactReducer.data)

    const [id, setId] = useState('')
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [age, setAge] = useState('')
    const [status, setStatus] = useState('create')
    const [filepath, setFilepath] = useState({})
    const [imageurl, setImageurl] = useState()
    const [loading, setLoading] = useState()

    useEffect(() => {
        if(route.params && route.params.msg === 'update'){
            const contactTarget = contacts.find(contact => contact.id === route.params.id)
            setId(contactTarget.id)
            setFirstname(contactTarget.firstName)
            setLastname(contactTarget.lastName)
            const age = contactTarget.age
            setAge(age.toString())
            setImageurl(contactTarget.photo)
            setStatus(route.params.msg)
        }  
    }, [route.params])

    const validation = () => {
        let messageError = contactValidation(firstname, lastname, age, imageurl)
        if(!messageError && status === 'update'){
            PutContact()
        }else if(!messageError){
            PostContact()
        }else{
            ToastAndroid.show(messageError, ToastAndroid.LONG);
        }
    }

    const PostContact = () => {
        const newContact = {
            firstName: firstname,
            lastName: lastname,
            age: age,
            photo: imageurl
        }
        postContact(newContact).then(response => {
            if(response.message === "contact saved"){
                navigation.navigate('Home')
                // dispatch(ClearContacts())
                dispatch(reloadContacts())
                ToastAndroid.show(response.message, ToastAndroid.LONG);
            }
        })
    }

    const PutContact = () => {
        const newContact = {
            firstName: firstname,
            lastName: lastname,
            age: age,
            photo: imageurl
        }
        putContact(newContact, id).then(response => {
            if(response.message === "Contact edited"){
                navigation.navigate('Home')
                // dispatch(ClearContacts())
                dispatch(reloadContacts())
                ToastAndroid.show(response.message, ToastAndroid.LONG);
            }
        })
    }

    const handleFirstname = (text) => {
        setFirstname(text)
    }

    const handleLastname = (text) => {
        setLastname(text)
    }

    const handleAge = (age) => {
        setAge(age)
    }

    const chooseFile = (type) => {

        launchImageLibrary({}, async (response) => {
            if(response && response.assets){
                setLoading(true)
                const asset = response.assets[0]
                const reference = storage().ref(`/photos/${asset.fileName}`);
                
                await reference.putFile(asset.uri)
                const url = await reference.getDownloadURL();
                setImageurl(url)
                setLoading(false)
            }
        })
    }

    return (
        <ScrollView>
            <View style={styles.main}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>Contact {status === 'update' ? 'Update' : 'Add'}</Text>
                </View>
                <View style={styles.mainBox}>
                    <View style={styles.loginFormBox}>
                        <View style={styles.fieldBox}>
                            <View style={styles.label}>
                                <Text>First Name</Text>
                            </View>
                            <View style={styles.textInputField}>
                                <TextInput 
                                    placeholder='first name'
                                    value={firstname}
                                    onChangeText={handleFirstname} />
                            </View>
                        </View>
                        <View style={styles.fieldBox}>
                            <View style={styles.label}>
                                <Text>Last Name</Text>
                            </View>
                            <View style={styles.textInputField}>
                                <TextInput 
                                    placeholder='last name' 
                                    value={lastname}
                                    onChangeText={handleLastname} />
                            </View>
                        </View>
                        <View style={styles.fieldBox}>
                            <View style={styles.label}>
                                <Text>Age</Text>
                            </View>
                            <View style={styles.textInputField}>
                                <TextInput 
                                    placeholder='age'
                                    value={age} 
                                    onChangeText={handleAge}
                                    keyboardType="numeric" />
                            </View>
                        </View>
                        <View style={styles.fieldBox}>
                            <TouchableOpacity
                                activeOpacity={0.5}
                                style={styles.buttonStyle}
                                onPress={() => chooseFile('photo')}>
                                <Text style={styles.textStyle}>Choose Image File</Text>
                            </TouchableOpacity>
                            {
                                loading? 
                                            <ActivityIndicator animating={true} size="small" color="#0000ff" />
                                        :
                                            <View style={styles.label}>
                                                <Image
                                                    style={styles.image}
                                                    source={{
                                                    uri: imageurl,
                                                    }}
                                                />
                                            </View>
                            }
                        </View>
                        <TouchableOpacity onPress={() => validation()} style={styles.btn} activeOpacity={0.8}>
                            <Text>{status === 'update' ? 'SAVE' : 'ADD'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: 'whitesmoke'
    },
    header: {
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 10,
        height: 30,
        borderBottomStartRadius:25,
        borderBottomEndRadius:25,
        backgroundColor: '#ec131c',
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20
    },
    mainBox:{
        flex:1,
        backgroundColor: 'whitesmoke',
        width:'100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24
    },
    loginFormBox: {
        height: 600,
        width: 350,
        padding: 4,
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: '#FFF5E2',
        borderRadius: 25
    },
    fieldBox: {
        alignItems: 'flex-start',
    },
    label: {
        marginVertical: 12
    },
    textInputField: {
        backgroundColor: 'white',
        color: 'black',
        borderRadius: 12,
        padding: 2,
        width: 250
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 24,
        elevation: 8,
        backgroundColor: "#13ECE3",
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        width: 250,
        height: 40
    },
    buttonStyle: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 5,
        marginVertical: 10,
        width: 250,
      },
      image: {
        width: 100,
        height: 100,
        borderRadius: 25,
        marginRight: 20
      },
})

export default ContactAdd