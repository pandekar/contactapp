import Homepage from './Homepage'
import ContactAdd from './ContactAdd'
import ContactDetail from './ContactDetail'

export {
    Homepage,
    ContactAdd,
    ContactDetail
}