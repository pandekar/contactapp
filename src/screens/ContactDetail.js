import React ,{useEffect, useState} from 'react'
import {View, Text, StyleSheet, Image } from 'react-native'
import { useRoute } from '@react-navigation/native';
import { getContact } from '../services/contact.service';

function ContactDetail(){

    const route = useRoute()

    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [age, setAge] = useState('')
    const [photo, setPhoto] = useState()

    useEffect(() => {
        getContact(route.params).then(res => {
            setFirstname(res.data.firstName)
            setLastname(res.data.lastName)
            const age = res.data.age
            setAge(age.toString())
            setPhoto(res.data.photo)
        })
    }, [route.params])

    return(
        <View style={styles.main}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Contact Detail</Text>
            </View>
            <View style={styles.mainBox}>
                <View style={styles.loginFormBox}>
                    <View>
                        {photo != "N/A" 
                            ?   
                                <Image
                                    style={styles.image}
                                    source={{
                                        uri: photo,
                                    }}
                                />
                            :   
                                <Image
                                    style={styles.image} 
                                    source={require('../images/unknown.png')} 
                                />                  
                        }
                    </View>
                    <View style={styles.fieldBox}>
                        <View style={styles.label}>
                            <Text style={styles.detailText}>First name : {firstname}</Text>
                        </View>
                    </View>
                    <View style={styles.fieldBox}>
                        <View style={styles.label}>
                            <Text style={styles.detailText}>Last name : {lastname}</Text>
                        </View>
                    </View>
                    <View style={styles.fieldBox}>
                        <View style={styles.label}>
                            <Text style={styles.detailText}>Age : {age}</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: 'whitesmoke'
    },
    header: {
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 10,
        height: 30,
        borderBottomStartRadius:25,
        borderBottomEndRadius:25,
        backgroundColor: '#ec131c',
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20
    },
    mainBox:{
        flex:1,
        backgroundColor: '#F04249',
        width:'100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24
    },
    loginFormBox: {
        height: 600,
        width: 350,
        padding: 4,
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: '#FFF5E2',
        borderRadius: 25
    },
    fieldBox: {
        alignItems: 'flex-start',
    },
    label: {
        marginVertical: 12
    },
    detailText:{
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold'
    },
    textInputField: {
        backgroundColor: 'white',
        color: 'black',
        borderRadius: 12,
        padding: 2,
        width: 250
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 24,
        elevation: 8,
        backgroundColor: "#13ECE3",
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        width: 250,
        height: 40
    },
    buttonStyle: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 5,
        marginVertical: 10,
        width: 250,
      },
      image: {
        width: 250,
        height: 250,
        borderRadius: 25,
        // marginRight: 20
      },
})

export default ContactDetail