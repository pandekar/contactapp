import { createStore, combineReducers, applyMiddleware } from 'redux'
// import ContactReducer from '../reducers/contact.reducer'
import contactReducer from '../reducers/contact.reducer'
import createSagaMiddleware from 'redux-saga'
import watchContact from '../sagas/contact.saga'

const sagaMiddleware = createSagaMiddleware()

// const Store = createStore(combineReducers({
//     contactReducer: ContactReducer,
// }))

const Store = createStore(combineReducers({
    contactReducer
}), applyMiddleware(sagaMiddleware))

sagaMiddleware.run(watchContact)

export default Store