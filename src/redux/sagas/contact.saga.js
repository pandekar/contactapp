import { takeLatest, call, put } from 'redux-saga/effects'

import { FETCH_CONTACT, fetchContactSuccess, fetchContactError } from '../actions/contact.action'

import { getContacts } from '../../services/contact.service'

function* fetchContact() {
    try{
        const contacts = yield call(getContacts)
        yield put(fetchContactSuccess(contacts.data))
    } catch(error){
        yield put(fetchContactError(error.message))
    }
}

function* watchContact() {
    yield takeLatest(FETCH_CONTACT, fetchContact)
}

export default watchContact