import {REFRESH_CONTACTS, 
        FETCH_CONTACT, 
        FETCH_CONTACT_SUCCESS, 
        FETCH_CONTACT_ERROR,
        UPDATE_CONTACT,
        UPDATE_CONTACT_SUCCESS,
        UPDATE_CONTACT_ERROR
} from "../actions/contact.action"

const initState = {
    data: [],
    dataAdd: {},
    loading: false,
    error: undefined,
    dataUpdate: {}
}

const contactReducer = (state = initState, action) => {
    switch(action.type){
        case REFRESH_CONTACTS:
            return{
                data: []
            }
        case FETCH_CONTACT:
            return{
                ...state, loading: true
            }
        case FETCH_CONTACT_SUCCESS:
            return{
                ...state, loading: false, data: action.data
            }
        case FETCH_CONTACT_ERROR:
            return{
                ...state, loading: false, error: action.error
            }
        case UPDATE_CONTACT:
            return{
                ...state, loading: true
            }
        case UPDATE_CONTACT_SUCCESS:
            const oldContacts = [...state.data]
            const newContacts = oldContacts.map(contact => {
                if(contact.id === action.dataUpdate.id){
                    contact.age = action.dataUpdate.age
                    contact.firstName = action.dataUpdate.firstName
                    contact.lastName = action.dataUpdate.lastName
                    contact.photo = action.dataUpdate.photo
                }
                return contact
            })
            return{
                ...state, data: newContacts
            }
        case UPDATE_CONTACT_ERROR:
            return{
                ...state, error: action.error
            }
        default:
            return state
    }
}

// const ContactReducer = (state = [] , action) => {
//     switch(action.type){
//         case 'INITIATE_CONTACTS' :
//             if(action.data) return[...state, ...action.data]
//         case 'CREATE_CONTACT' :
//             return [...state, action.data]
//         case 'CLEAR_CONTACTS' :
//             return []
//         case 'UPDATE_CONTACT' : 
//             const oldContacts = [...state]
//             const newContacts = oldContacts.map ( contact => {
//                 if(contact.id === action.data.id){
//                     contact.age = action.data.age
//                     contact.firstName = action.data.firstName
//                     contact.lastName = action.data.lastName
//                     contact.photo = action.data.photo
//                 }
//                 return contact
//             })
//             return newContacts
//         case 'DELETE_CONTACT' :
//             const contactsBefore = [...state]
//             const contactsAfter = contactsBefore.filter( contact => contact.id != action.data.id )
//             return contactsAfter
//         default:
//             return state
//     }
// }

// export default ContactReducer
export default contactReducer