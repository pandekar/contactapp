export const REFRESH_CONTACTS = 'REFRESH_CONTACTS'

export const FETCH_CONTACT = 'FETCH_CONTACTS'
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACTS_SUCCESS'
export const FETCH_CONTACT_ERROR = 'FETCH_CONTACTS_ERROR'

export const UPDATE_CONTACT = 'UPDATE_CONTACT'
export const UPDATE_CONTACT_SUCCESS = 'UPDATE_CONTACT_SUCCESS'
export const UPDATE_CONTACT_ERROR = 'UPDATE_CONTACT_ERROR'

const reloadContacts = (data) => {
    return{
        type: REFRESH_CONTACTS,
        data
    }
}

const fetchInitiateContact = () => {
    return{
        type: FETCH_CONTACT
    }
}

const fetchContactSuccess = (data) => {
    return{
        type: FETCH_CONTACT_SUCCESS,
        data
    }
}

const fetchContactError = (error) => {
    return{
        type: FETCH_CONTACT_ERROR,
        error
    }
}

const updateContact = (contact, id) => {
    return{
        type: UPDATE_CONTACT,
        data: {
            contact,
            id
        }
    }
}

const updateContactSuccess = (contact) => {
    return{
        type: UPDATE_CONTACT_SUCCESS,
        dataUpdate: contact
    }
}

const updateContactError = (error) => {
    return{
        type: UPDATE_CONTACT_ERROR,
        error
    }
}

// const InitiateContacts = (contacts) => {
//     return{
//         type: 'INITIATE_CONTACTS',
//         data: contacts
//     }
// }

// const ClearContacts = () => {
//     return{
//         type: 'CLEAR_CONTACTS',
//         data: []
//     }
// }

// const CreateContact = (contact) => {
//     return{
//         type: 'CREATE_CONTACT',
//         data: contact
//     }
// }

// const UpdateContact = (contact) => {
//     return{
//         type: 'UPDATE_CONTACT',
//         data: contact
//     }
// }

// const DeleteContact = (contact) => {
//     return{
//         type: 'DELETE_CONTACT',
//         data: contact
//     }
// }

// export { InitiateContacts, CreateContact, UpdateContact, DeleteContact, ClearContacts }
export { reloadContacts, fetchInitiateContact, fetchContactSuccess, fetchContactError, 
            updateContact, updateContactSuccess, updateContactError }