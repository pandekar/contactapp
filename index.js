/**
 * @format
 */

import React from 'react'
import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';
import Router from './src/router/index'
import {NavigationContainer} from '@react-navigation/native'
import store from './src/redux/store/store'
import {Provider} from 'react-redux'

function App(){
    return(
        <Provider store={store}>
            <NavigationContainer>
                <Router/>
            </NavigationContainer>
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => App);
